const std = @import("std");

// const Stringer = @import("to_string_tagged.zig").Stringer;

const Stringer = @import("to_string_ptr.zig").Stringer;
const Animal = @import("to_string_ptr.zig").Animal;
const User = @import("to_string_ptr.zig").User;

fn printStringer(s: Stringer) !void {
    var buf: [256]u8 = undefined;
    const str = try s.toString(&buf);
    std.debug.print("{s}\n", .{str});
}

pub fn main() !void {
    // Tagged union interface.
    // var bob = Stringer{ .user = .{
    //     .name = "Bob",
    //     .email = "bob@example.com",
    // } };
    // try printStringer(bob);
    //
    // var donald = Stringer{ .animal = .{
    //     .name = "Donald Duck",
    //     .greeting = "Quack!",
    // } };
    // try printStringer(donald);

    // Pointer cast interface.
    var bob = User{
        .name = "Bob",
        .email = "bob@example.com",
    };
    const bob_impl = bob.stringer();
    try printStringer(bob_impl);

    var donald = Animal{
        .name = "Donald Duck",
        .greeting = "Quack!",
    };
    const donald_impl = donald.stringer();
    try printStringer(donald_impl);
}
