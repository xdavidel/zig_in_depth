const std = @import("std");

// Import the generated Zig file. We are
// guaranteed that this will be available since
// the main executable build step depends on the
// code gen and file writing build steps.
const fibs = @import("fibs.zig").fibs;

pub fn main() !void {
    for (&fibs, 1..) |n, i| std.debug.print("fib {}: {}\n", .{ i, n });
}
