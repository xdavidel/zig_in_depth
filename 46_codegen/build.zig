const std = @import("std");

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    // Add a build option to specify the Fibonacci end.
    // This is passed to zig build via the -D flag.
    const fib_end = b.option(
        []const u8,
        "fib-end",
        "End of the Fibonnaci sequence to calculate.",
    ) orelse "7";

    // Compile the executable that will generate
    // the Zig code.
    const gen_exe = b.addExecutable(.{
        .name = "gen_exe",
        .root_source_file = .{ .path = "src/gen.zig" },
        .target = target,
        .optimize = optimize,
    });

    // Run the code gen executable after compiling it.
    const run_gen_exe = b.addRunArtifact(gen_exe);
    run_gen_exe.step.dependOn(&gen_exe.step);

    // This creates an argument to be passed to the
    // code gen executable, telling it the filename
    // to use when generating the Zig code.
    // The generated output file with Zig code will
    // be available in the `output_file` const for further
    // use in the build process. The actual file is placed
    // within the build system cache directory structure.
    const output_file = run_gen_exe.addOutputFileArg("fibs.zig");

    // This passes in the fib-end build arg as a
    // command line arg for the code gen executable.
    run_gen_exe.addArg(fib_end);

    // Now we want to write out the generated Zig code
    // as a file in the src directory.
    const gen_write_files = b.addWriteFiles();

    // Here we use the generated output from the code gen
    // step and specify where to write it in the project.
    gen_write_files.addCopyFileToSource(output_file, "src/fibs.zig");

    // The main application executable.
    const main_exe = b.addExecutable(.{
        .name = "main",
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });

    // We make clear that you can't run the main
    // application without performing the code gen first.
    // This is necessary since Zig can build independent
    // steps in parallel.
    main_exe.step.dependOn(&gen_write_files.step);

    // Install (write) the main executable file to zig-out/bin.
    b.installArtifact(main_exe);

    // To run the main executable we depend on
    // installing / writing the files first.
    const run_cmd = b.addRunArtifact(main_exe);
    run_cmd.step.dependOn(b.getInstallStep());

    // The actual run step made available to zig build.
    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);
}
