const std = @import("std");

const zg = @import("ziglyph");

pub fn main() !void {
    std.debug.print("{u}\n", .{zg.toLower('A')});
}
