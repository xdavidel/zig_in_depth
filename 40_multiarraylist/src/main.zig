const std = @import("std");

// This Zig struct uses 16 bytes (with padding) in an array.
// In a `MultiArrayList` it would use 12 bytes (no padding needed).
const Foo = struct {
    a: u16,
    b: u64,
    c: u16,
};

pub fn main() !void {
    // Set up our allocator.
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    const allocator = gpa.allocator();

    // Init our multi-array list.
    var multi = std.MultiArrayList(Foo){};
    defer multi.deinit(allocator);

    // Add an item, allocating if necessary.
    try multi.append(allocator, .{ .a = 1, .b = 1, .c = 1 });

    // Pre-allocate to add more items.
    try multi.ensureUnusedCapacity(allocator, 2);
    // Now we can add without the allocator or the `try`.
    multi.appendAssumeCapacity(.{ .a = 2, .b = 2, .c = 2 });
    multi.appendAssumeCapacity(.{ .a = 3, .b = 3, .c = 3 });

    // You can get a slice of a specific field from all the
    // items in the list with `items`.
    std.debug.print(".a: {any}\n", .{multi.items(.a)});
    std.debug.print(".b: {any}\n", .{multi.items(.b)});
    std.debug.print(".c: {any}\n\n", .{multi.items(.c)});

    // If you will be accessing more than one field, it's
    // better to get the slice of all fields first, and then
    // call `items` on that. This provides better performance.
    const sliced = multi.slice();
    const a_fields = sliced.items(.a);
    const b_fields = sliced.items(.b);
    const c_fields = sliced.items(.c);
    std.debug.print("first .a = {}\n", .{a_fields[0]});
    std.debug.print("second .b = {}\n", .{b_fields[1]});
    std.debug.print("third .c = {}\n\n", .{c_fields[2]});
    // And that's one way to iterate over a field for all items.
    for (a_fields, 0..) |a, i| std.debug.print("{}: .a = {}\n", .{ i, a });
    for (b_fields, 0..) |b, i| std.debug.print("{}: .b = {}\n", .{ i, b });
    for (c_fields, 0..) |c, i| std.debug.print("{}: .c = {}\n", .{ i, c });
    std.debug.print("\n", .{});

    // You can get an item at an index in the list.
    const first_foo = multi.get(0);
    std.debug.print("first foo: {any}\n", .{first_foo});
    // You can set an item at an index in the list.
    // This overwrites the existing item at that index.
    multi.set(1, .{ .a = 4, .b = 4, .c = 4 });

    // The slices obtained above reflect the new change since
    // they are just pointers to the list's data.
    for (a_fields, 0..) |a, i| std.debug.print("{}: .a = {}\n", .{ i, a });
    for (b_fields, 0..) |b, i| std.debug.print("{}: .b = {}\n", .{ i, b });
    for (c_fields, 0..) |c, i| std.debug.print("{}: .c = {}\n", .{ i, c });
    std.debug.print("\n", .{});

    // As with `ArrayList` you can use `MultiArrayList` as a stack.
    const head = multi.pop();
    std.debug.print("head: {any}\n", .{head});
    try multi.append(allocator, .{ .a = 5, .b = 5, .c = 5 }); // push
    // `popOrNull` to use the list as an iterator with `while`.
    var i: usize = 0;

    while (multi.popOrNull()) |item| : (i += 1)
        std.debug.print("items[{}]: {any}\n", .{ i, item });

    std.debug.print("\n", .{});

    // Beware! If the list structure is modified, the previously
    // obtained slices are invalidated. This may occur when appending
    // or removing / popping items.
    std.debug.print("a_fields.len: {}\n", .{a_fields.len});
    std.debug.print("b_fields.len: {}\n", .{b_fields.len});
    std.debug.print("c_fields.len: {}\n", .{c_fields.len});
    std.debug.print("list len: {}\n", .{multi.items(.a).len});
}
