const std = @import("std");

const A = struct {
    // Method
    fn toString(_: A) []const u8 {
        return "A";
    }
};

const B = struct {
    // Not a method.
    fn toString(s: []const u8) []const u8 {
        return s;
    }
};

const C = struct {
    // Not even a function.
    const toString: []const u8 = "C";
};

const D = enum {
    a,
    d,

    // Method
    fn toString(self: D) []const u8 {
        return @tagName(self);
    }
};

fn print(x: anytype) void {
    const T = @TypeOf(x);
    // Do we have a "toString" declaration?
    if (!@hasDecl(T, "toString")) return;
    // Is it a function?
    const DT = @TypeOf(@field(T, "toString"));
    if (@typeInfo(DT) != .Fn) return;
    // Is it a method?
    const args = std.meta.ArgsTuple(DT);
    inline for (std.meta.fields(args), 0..) |arg, i| {
        if (i == 0 and arg.type == T) {
            // Now we know we can call it as a method.
            std.debug.print("{s}\n", .{x.toString()});
        }
    }

    // Or an easier way...
    const decl_info = @typeInfo(DT);
    if (decl_info.Fn.params.len > 0) {
        if (decl_info.Fn.params[0].type) |PT| {
            if (PT == T) std.debug.print("{s}\n", .{x.toString()});
        }
    }
}

pub fn main() !void {
    const a = A{};
    const b = B{};
    const c = C{};
    const d = D.d;

    print(a);
    print(b);
    print(c);
    print(d);
}
