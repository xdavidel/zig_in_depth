const std = @import("std");

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    // Use command line options or default to native target.
    const exe = b.addExecutable(.{
        .name = "main",
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });
    b.installArtifact(exe);

    // Convenience zig build run command.
    const run_cmd = b.addRunArtifact(exe);
    run_cmd.step.dependOn(b.getInstallStep());
    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    // Cross compile to Linux x86_64 musl ABI
    // Release Safe optimization level
    const exe_linux = b.addExecutable(.{
        .name = "main_linux",
        .root_source_file = .{ .path = "src/main.zig" },
        .target = .{
            .cpu_arch = .x86_64,
            .os_tag = .linux,
            .abi = .musl,
        },
        .optimize = .ReleaseSafe,
    });
    b.installArtifact(exe_linux);

    // Cross compile to Windows x86_64 GNU ABI
    // Release Small optimization level
    const exe_win = b.addExecutable(.{
        .name = "main_win",
        .root_source_file = .{ .path = "src/main.zig" },
        .target = .{
            .cpu_arch = .x86_64,
            .os_tag = .windows,
            .abi = .gnu,
        },
        .optimize = .ReleaseSmall,
    });
    b.installArtifact(exe_win);

    // Cross compile main.c to Windows x86_64 GNU ABI
    // Release Fast optimization level
    // Link the C standard library (libc)
    const exe_c_win = b.addExecutable(.{
        .name = "main_c_win",
        .root_source_file = .{ .path = "src/main.c" },
        .target = .{
            .cpu_arch = .x86_64,
            .os_tag = .windows,
            .abi = .gnu,
        },
        .optimize = .ReleaseFast,
    });
    exe_c_win.linkLibC();
    b.installArtifact(exe_c_win);
}
